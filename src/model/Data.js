import Images from "../constant/Images";

export default Data = [
    { index: 0, color: "white", image: Images.chess_piece_2_black_rook },
    { index: 1, color: "black", image: Images.chess_piece_2_black_knight },
    { index: 2, color: "white", image: Images.chess_piece_2_black_bishop },
    { index: 3, color: "black", image: Images.chess_piece_2_black_king },
    { index: 4, color: "white", image: Images.chess_piece_2_black_queen },
    { index: 5, color: "black", image: Images.chess_piece_2_black_bishop },
    { index: 6, color: "white", image: Images.chess_piece_2_black_knight },
    { index: 7, color: "black", image: Images.chess_piece_2_black_rook },
    { index: 8, color: "black", image: Images.chess_piece_2_black_pawn },
    { index: 9, color: "white", image: Images.chess_piece_2_black_pawn },
    { index: 10, color: "black", image: Images.chess_piece_2_black_pawn },
    { index: 11, color: "white", image: Images.chess_piece_2_black_pawn },
    { index: 12, color: "black", image: Images.chess_piece_2_black_pawn },
    { index: 13, color: "white", image: Images.chess_piece_2_black_pawn },
    { index: 14, color: "black", image: Images.chess_piece_2_black_pawn },
    { index: 15, color: "white", image: Images.chess_piece_2_black_pawn },
    { index: 16, color: "white", image: "" },
    { index: 17, color: "black", image: "" },
    { index: 18, color: "white", image: "" },
    { index: 19, color: "black", image: "" },
    { index: 20, color: "white", image: "" },
    { index: 21, color: "black", image: "" },
    { index: 22, color: "white", image: "" },
    { index: 23, color: "black", image: "" },
    { index: 24, color: "black", image: "" },
    { index: 25, color: "white", image: "" },
    { index: 26, color: "black", image: "" },
    { index: 27, color: "white", image: "" },
    { index: 28, color: "black", image: "" },
    { index: 29, color: "white", image: "" },
    { index: 30, color: "black", image: "" },
    { index: 31, color: "white", image: "" },
    { index: 32, color: "white", image: "" },
    { index: 33, color: "black", image: "" },
    { index: 34, color: "white", image: "" },
    { index: 35, color: "black", image: "" },
    { index: 36, color: "white", image: "" },
    { index: 37, color: "black", image: "" },
    { index: 38, color: "white", image: "" },
    { index: 39, color: "black", image: "" },
    { index: 40, color: "black", image: "" },
    { index: 41, color: "white", image: "" },
    { index: 42, color: "black", image: "" },
    { index: 43, color: "white", image: "" },
    { index: 44, color: "black", image: "" },
    { index: 45, color: "white", image: "" },
    { index: 46, color: "black", image: "" },
    { index: 47, color: "white", image: "" },
    { index: 48, color: "white", image: Images.chess_piece_2_white_pawn },
    { index: 49, color: "black", image: Images.chess_piece_2_white_pawn },
    { index: 50, color: "white", image: Images.chess_piece_2_white_pawn },
    { index: 51, color: "black", image: Images.chess_piece_2_white_pawn },
    { index: 52, color: "white", image: Images.chess_piece_2_white_pawn },
    { index: 53, color: "black", image: Images.chess_piece_2_white_pawn },
    { index: 54, color: "white", image: Images.chess_piece_2_white_pawn },
    { index: 55, color: "black", image: Images.chess_piece_2_white_pawn },
    { index: 56, color: "black", image: Images.chess_piece_2_white_rook },
    { index: 57, color: "white", image: Images.chess_piece_2_white_knight },
    { index: 58, color: "black", image: Images.chess_piece_2_white_bishop },
    { index: 59, color: "white", image: Images.chess_piece_2_white_king },
    { index: 60, color: "black", image: Images.chess_piece_2_white_queen },
    { index: 61, color: "white", image: Images.chess_piece_2_white_bishop },
    { index: 62, color: "black", image: Images.chess_piece_2_white_knight },
    { index: 63, color: "white", image: Images.chess_piece_2_white_rook },
];