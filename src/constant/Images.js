const images = {
  
  chess_piece_2_black_bishop: require("../assets/Images/chess_piece_2_black_bishop.png"),
  chess_piece_2_black_king: require("../assets/Images/chess_piece_2_black_king.png"),
  chess_piece_2_black_knight: require("../assets/Images/chess_piece_2_black_knight.png"),
  chess_piece_2_black_pawn: require("../assets/Images/chess_piece_2_black_pawn.png"),
  chess_piece_2_black_queen: require("../assets/Images/chess_piece_2_black_queen.png"),
  chess_piece_2_black_rook: require("../assets/Images/chess_piece_2_black_rook.png"),
  chess_piece_2_white_bishop: require("../assets/Images/chess_piece_2_white_bishop.png"),
  chess_piece_2_white_king: require("../assets/Images/chess_piece_2_white_king.png"),
  chess_piece_2_white_knight: require("../assets/Images/chess_piece_2_white_knight.png"),
  chess_piece_2_white_pawn: require("../assets/Images/chess_piece_2_white_pawn.png"),
  chess_piece_2_white_queen: require("../assets/Images/chess_piece_2_white_queen.png"),
  chess_piece_2_white_rook: require("../assets/Images/chess_piece_2_white_rook.png"),
  

};

export default images