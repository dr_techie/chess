import React, { useEffect } from 'react';
import {
  Dimensions, FlatList, Image, LogBox, SafeAreaView, Text, TouchableOpacity, View
} from 'react-native';
import AlphabetCard from '../components/AlphabetCard';
import global_style from '../components/GlobalStyle';
import NumericCard from '../components/NumericCard';
import StatusBarComponent from '../components/StatusBarComponent';
import Theme from '../components/Theme';
import Images from "../constant/Images";
import Data from '../model/Data';
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;


const ChessScreen = (props) => {

  const [random, setRandom] = React.useState('');
  const [dataArray, setDataArray] = React.useState([]);
  const [tempArray, setTempArray] = React.useState([]);
  const [play, setPlay] = React.useState(true);
  const forceUpdate = React.useReducer(bool => !bool)[1];


  useEffect(() => {

    //copy Data arry to data
    let data  =[...Data];
    setDataArray(data)

    // temp arr
    let temp = [...Data]
    setTempArray(temp)


  }, [])


  const clickRandom = () => {

    if (play) {
      var getRandomValue = getRandomNumberBetween(8, 15)
      setPlay(false)
      dataArray[getRandomValue + 8].image = dataArray[getRandomValue].image;
      dataArray[getRandomValue].image = "";
      forceUpdate()

    } else {
      setPlay(true)
      var getRandomValue = getRandomNumberBetween(48, 55)
      dataArray[getRandomValue - 8].image = dataArray[getRandomValue].image;
      dataArray[getRandomValue].image = "";
      forceUpdate()
    }

  }


  function getRandomNumberBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }


  const clickReset = () => {
   
    setDataArray(tempArray)
    forceUpdate()
  }

  return (
    <SafeAreaView style={[global_style.Container]}>

      <StatusBarComponent global_navigation={props} />

      <View style={[global_style.Center]}>

        <View style={{ width: '100%', backgroundColor: Theme.color_black }}>

          <AlphabetCard />

          <View style={{ flexDirection: 'row' }}>


            <NumericCard />

            <FlatList
              data={dataArray}
              numColumns={8}
              style={{ alignSelf: 'center', }}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) =>

                <View style={{
                  backgroundColor: item.color, width: deviceWidth / 8.7, height: 50,
                  alignItems: 'center', justifyContent: 'center'
                }}>

                  <Image
                    source={item.image}
                    style={{ width: 35, height: 35 }} />

                </View>

              } />

            <NumericCard />

          </View>

          <AlphabetCard />

        </View>


        <TouchableOpacity
          activeOpacity={0.7}
          style={{ marginTop: 40, width: deviceWidth / 3 }}
          onPress={() => clickRandom()}>

          <View style={{ backgroundColor: Theme.color_primary, borderRadius: 6, justifyContent: 'center' }}>
            <Text style={[global_style.medium16, { padding: 6, textAlign: 'center' }]}>{"Random"}</Text>
          </View>
        </TouchableOpacity>


        <TouchableOpacity
          activeOpacity={0.7}
          style={{ marginTop: 20, width: deviceWidth / 3 }}
          onPress={() => clickReset()}>

          <View style={{ backgroundColor: Theme.color_primary, borderRadius: 6, justifyContent: 'center' }}>
            <Text style={[global_style.medium16, { padding: 6, textAlign: 'center' }]}>{"Reset"}</Text>
          </View>
        </TouchableOpacity>

      </View>


    </SafeAreaView>
  );
}

export default ChessScreen;