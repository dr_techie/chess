import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import ChessScreen from '../container/ChessScreen';



const RootStack = createStackNavigator();

const RootStackScreen = ({ navigation }) => (

  <RootStack.Navigator headerMode='none'>
    <RootStack.Screen name="ChessScreen" component={ChessScreen} />
  </RootStack.Navigator>

);

export default RootStackScreen;