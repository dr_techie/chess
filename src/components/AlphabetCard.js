import React from 'react';
import { View, Text, Image, StyleSheet, Pressable, Dimensions } from 'react-native';
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
import global_style from '../components/GlobalStyle';

const AlphabetCard = ({}) => {

    return (
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 12 }}>

            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"A"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"B"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"C"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"D"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"E"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"F"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"G"}</Text>
            <Text style={[global_style.bold16, { textAlign: 'center', flex: 1 }]}>{"H"}</Text>

        </View>
    );
};

export default AlphabetCard;

const styles = StyleSheet.create({
    card: {
        height: 100,
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%'
    },
    cardImgWrapper: {
        flex: 1,
    },
    cardInfo: {
        flex: 2,
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderBottomRightRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
    },
    cardTitle: {
        fontFamily: "HKGrotesk-Bold",
        fontSize: 17,
    },
    cardDetails: {
        fontSize: 12,
        color: '#444',
    },
});
