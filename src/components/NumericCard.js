import React from 'react';
import { View, Text, Image, StyleSheet, Pressable, Dimensions } from 'react-native';
export const deviceHeight = Dimensions.get('window').height;
export const deviceWidth = Dimensions.get('window').width;
import global_style from '../components/GlobalStyle';

const NumericCard = ({ itemData, onPress }) => {

    return (
        <View style={{ flexDirection: 'column', justifyContent: 'space-between', width: 12, marginHorizontal: 2 }}>

            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"8"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"7"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"6"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"5"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"4"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"3"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"2"}</Text>
            <Text style={[global_style.bold16, { textAlignVertical: 'center', flex: 1 }]}>{"1"}</Text>

        </View>
    );
};

export default NumericCard;

const styles = StyleSheet.create({
    card: {
        height: 100,
        marginVertical: 10,
        flexDirection: 'row',
        width: '100%'
    },
    cardImgWrapper: {
        flex: 1,
    },
    cardInfo: {
        flex: 2,
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderBottomRightRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: '#fff',
    },
    cardTitle: {
        fontFamily: "HKGrotesk-Bold",
        fontSize: 17,
    },
    cardDetails: {
        fontSize: 12,
        color: '#444',
    },
});
